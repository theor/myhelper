//
//  DeadlineModel.h
//  MyHelper
//
//  Created by svp on 22/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DeadlineModel : NSObject {
    
}

@property (nonatomic) int deadlineId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSDate *date;

- (id)initWithData:(int)newId name:(NSString*)newName date:(NSDate*)newDate;

@end
