//
//  NoteModel.h
//  MyHelper
//
//  Created by svp on 21/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NoteModel : NSObject {
    
}

@property (nonatomic) int noteId;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *description;

@end
