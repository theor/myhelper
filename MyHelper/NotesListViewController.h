//
//  NotesListViewController.h
//  MyHelper
//
//  Created by svp on 20/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MTISqliteManager.h"
#import "DropBoxHelper.h"
#import "NoteModel.h"
#import "CalendarTableViewCell.h"

@interface NotesListViewController : UIViewController {
    bool firstLoad;
}

@property (nonatomic, retain) NSMutableArray *data;
@property (nonatomic, retain) IBOutlet UITableView *tableView;

- (IBAction)addButtonclick:(id)sender;
- (void)addModel:(NoteModel*)model;

@end
