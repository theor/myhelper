//
//  DropBoxHelper.h
//  MyHelper
//
//  Created by svp on 29/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DropboxSDK.h"
#import "MTIFileManager.h"


@interface DropBoxHelper : NSObject {
    id<DBLoginControllerDelegate, DBRestClientDelegate> delegate;
    DBRestClient* client;
}

+ (DropBoxHelper*) Get: (id<DBLoginControllerDelegate, DBRestClientDelegate>) delegate;

- (void) Link: (UIViewController*) view;
- (void) Unlink;
- (void) LoadAccountInfo;
- (void) Push;
- (void) Pull;
- (void) SqliteMetadata;

@property (nonatomic, assign) id<DBLoginControllerDelegate, DBRestClientDelegate> delegate;

@property (nonatomic, retain) DBRestClient* client;

@end

