//
//  NotesDetailsViewController.m
//  MyHelper
//
//  Created by svp on 20/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NotesDetailsViewController.h"


@implementation NotesDetailsViewController

@synthesize model;
@synthesize titleLabel;
@synthesize textView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [model release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Details";
    
    UIBarButtonItem *sendButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", @"")                                                         style:UIBarButtonItemStyleBordered  target:self action:@selector(editButtonclick:)] autorelease];
    
    self.navigationItem.rightBarButtonItem = sendButton;
    self.titleLabel.text = model.title;
    self.textView.text = model.description;
}

- (IBAction)editButtonclick:(id)sender {
    NotesAddViewController* notes = [[NotesAddViewController alloc] init];
    notes.model = model;
    [[self navigationController] pushViewController:notes animated:YES];
    [notes release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end
