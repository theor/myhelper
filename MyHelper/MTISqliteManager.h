//
//  MTISqliteManager.h
//  TP1
//
//  Created by Plopix on 11/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#define BDD_FILENAME @"MyHelper.sqlite"

@interface MTISqliteManager : NSObject {
 	sqlite3* database;
	NSMutableDictionary* compiledQueries;
   
}

+(MTISqliteManager*) sharedInstance;
- (sqlite3_stmt*) compileQueryOrGetFromCache:(NSString*) query;
- (sqlite3_stmt*) compileQuery:(NSString*) query;

- (void) copyDbFromRessourceIfNeeded;
- (void) openDatabase;
- (NSInteger) lastInsertId;
- (bool) reloadIfNeeded;


@end
