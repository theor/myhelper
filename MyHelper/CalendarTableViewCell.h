//
//  CalendarTableViewCell.h
//  MyHelper
//
//  Created by svp on 04/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CalendarTableViewCell : UITableViewCell {
    
    UILabel *labelTwo;
    UILabel *labelOne;
}
@property (nonatomic, retain) IBOutlet UILabel *labelTwo;
@property (nonatomic, retain) IBOutlet UILabel *labelOne;

@end
