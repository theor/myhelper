//
//  DropBoxHelper.m
//  MyHelper
//
//  Created by svp on 29/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DropBoxHelper.h"


static DropBoxHelper* _instance = nil;
static NSString* _dbPath = nil;

@interface DropBoxHelper () 
    - (void) createClient;
//@private

@end

@implementation DropBoxHelper

@synthesize delegate;
@synthesize client;



+ (DropBoxHelper*) Get:(id<DBLoginControllerDelegate, DBRestClientDelegate>) delegate{
    if (_instance == nil)
    {
        _instance = [DropBoxHelper alloc];
        _dbPath = [[NSString alloc] initWithFormat:@"%@/MyHelper.sqlite", [MTIFileManager getPathIn:APP_DOCUMENTS]];

    }
    _instance.delegate = delegate;

    return _instance;
}

- (void) Link: (UIViewController*) view {
    if (![[DBSession sharedSession] isLinked]) {
        DBLoginController* controller = [[DBLoginController new] autorelease];
        controller.delegate = delegate;
        [controller presentFromController:view];
    } 
    
}

- (void) Unlink{
    if ([[DBSession sharedSession] isLinked]){
        [[DBSession sharedSession] unlink];
        [[[[UIAlertView alloc] 
           initWithTitle:@"Account Unlinked!" message:@"Your dropbox account has been unlinked" 
           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
          autorelease]
         show];
    }

}

- (void) LoadAccountInfo{
    [self createClient];
    [client loadAccountInfo];
}

- (void) Push{
    if (![[DBSession sharedSession] isLinked])
        return;
    NSLog(@"Pushing");
    [self createClient];
    [client uploadFile:@"MyHelper.sqlite" toPath:@"" fromPath:_dbPath];
}

- (void) Pull{
    if (![[DBSession sharedSession] isLinked])
        return;
    NSLog(@"Pulling");
    
    [self createClient];
    [client loadFile:@"/MyHelper.sqlite" intoPath:_dbPath];
}

- (void) SqliteMetadata{
    if (![[DBSession sharedSession] isLinked])
        return;
    [self createClient];
    [client loadMetadata:@"/MyHelper.sqlite"];
}

#pragma mark private methods

- (void) createClient {
    if (client == nil){
        client = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        client.delegate = delegate;
    }
}

@end
