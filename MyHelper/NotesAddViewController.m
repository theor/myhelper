//
//  NotesAddViewController.m
//  MyHelper
//
//  Created by svp on 20/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NotesAddViewController.h"

@implementation NotesAddViewController

@synthesize model;
@synthesize titleField;
@synthesize textView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [model release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Note";
    
    UIBarButtonItem *sendButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", @"")                                                         style:UIBarButtonItemStyleBordered  target:self action:@selector(saveButtonclick:)] autorelease];
    
    self.navigationItem.rightBarButtonItem = sendButton;
    
    if (model != NULL)
    {
        titleField.text = model.title;
        textView.text = model.description;
    }
    else
    {
        [self clearInputs];
    }
}

- (IBAction)saveButtonclick:(id)sender {
    
    if (model != NULL)
    {
        model.title = titleField.text;
        model.description = textView.text;
        
        NSString *query = @"UPDATE notes SET title = ?, description = ? where id = ? ";
        
        sqlite3_stmt* compiledQuery = [[MTISqliteManager sharedInstance] compileQuery:query];
        sqlite3_bind_text(compiledQuery, 1, [model.title UTF8String], -1, SQLITE_STATIC);
        sqlite3_bind_text(compiledQuery, 2, [model.description UTF8String], -1, SQLITE_STATIC);
        sqlite3_bind_int(compiledQuery, 3, model.noteId);
        sqlite3_step(compiledQuery);
        sqlite3_finalize(compiledQuery);
    }
    else if (parent != NULL)
    {
        NSString *query = @"INSERT INTO notes(title, description) VALUES(?, ?)";
        
        sqlite3_stmt* compiledQuery = [[MTISqliteManager sharedInstance] compileQuery:query];
        sqlite3_bind_text(compiledQuery, 1, [titleField.text UTF8String], -1, SQLITE_STATIC);
        sqlite3_bind_text(compiledQuery, 2, [textView.text UTF8String], -1, SQLITE_STATIC);
        sqlite3_step(compiledQuery);
        sqlite3_finalize(compiledQuery);
        
        /*NoteModel *newModel = [[NoteModel alloc] init]; 
        newModel.title = titleField.text;
        newModel.description = textView.text;
        newModel.noteId = [[MTISqliteManager sharedInstance] lastInsertId];
        
        [parent addModel:newModel];
        [newModel release];*/
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:TRUE forKey:@"notes_changed"];
    
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

- (void)setParent:(NotesListViewController*)newParent
{
    parent = newParent;
}

- (void)clearInputs
{
    [titleField setText:@""];
    [textView setText:@""];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end
