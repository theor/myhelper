//
//  CalendarListViewController.m
//  MyHelper
//
//  Created by svp on 21/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CalendarListViewController.h"
#import "CalendarAddViewController.h"

@interface CalendarListViewController () <DBLoginControllerDelegate, DBRestClientDelegate>
@end

@implementation CalendarListViewController
@synthesize listView;
@synthesize listViewData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        DeadlineModel *model = [listViewData objectAtIndex:indexPath.row];
        NSString *query = @"DELETE FROM calendar where id = ?";
        
        sqlite3_stmt* compiledQuery = [[MTISqliteManager sharedInstance] compileQuery:query];
        sqlite3_bind_int(compiledQuery, 1, model.deadlineId);
        sqlite3_step(compiledQuery);
        sqlite3_finalize(compiledQuery);
        
        [listViewData removeObject:model];
        [listView reloadData];
        
        [[DropBoxHelper Get:self] Push];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:TRUE forKey:@"today_changed"];
    }
}

- (void)dealloc
{
    [listView release];
    [listViewData release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.listViewData = [[NSMutableArray alloc] init];
    
    firstLoad = true;
    
    self.title = @"Calendar";
    
    UIBarButtonItem *addButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Add", @"") style:UIBarButtonItemStyleBordered  target:self action:@selector(addButtonclick:)] autorelease];
    self.navigationItem.rightBarButtonItem = addButton;
    
    // First load
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"calendar_changed"];
}

- (IBAction)addEvent:(DeadlineModel*)deadline
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZ"]; 
    
    NSString *query = @"INSERT INTO calendar(name, date) VALUES(?, ?)";
    
    sqlite3_stmt* compiledQuery = [[MTISqliteManager sharedInstance] compileQuery:query];
    sqlite3_bind_text(compiledQuery, 1, [deadline.name UTF8String], -1, SQLITE_STATIC);
    sqlite3_bind_text(compiledQuery, 2, [[dateFormatter stringFromDate:deadline.date] UTF8String], -1, SQLITE_STATIC);
    sqlite3_step(compiledQuery);
    sqlite3_finalize(compiledQuery);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:TRUE forKey:@"calendar_changed"];
    [defaults setBool:TRUE forKey:@"today_changed"];
    
    [dateFormatter release];
}

- (void)viewDidUnload
{
    [self setListView:nil];
    [self setListViewData:nil];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [super viewDidUnload];
}

- (IBAction)addButtonclick:(id)sender {
    CalendarAddViewController* addView = [[CalendarAddViewController alloc] init];
    [[self navigationController] pushViewController:addView animated:YES];
    [addView setParent: self];
    [addView release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listViewData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    CalendarTableViewCell *cell = (CalendarTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSArray *components = [[NSBundle mainBundle] loadNibNamed:@"CalendarTableViewCell" owner:nil options:nil];
    if (cell == nil) {
        for (id viewcell in components) {
            if ([viewcell isKindOfClass:[CalendarTableViewCell class]])
            {
                cell = viewcell;
                break;
            }
        }
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    DeadlineModel* model = [listViewData objectAtIndex:indexPath.row];
    cell.labelOne.text = model.name;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM. yyyy 'at' HH:mm"];
    cell.labelTwo.text = [formatter stringFromDate:model.date];
    [formatter release];
    
    return cell;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    bool reload = [[MTISqliteManager sharedInstance] reloadIfNeeded];
    bool changed = [[NSUserDefaults standardUserDefaults] boolForKey:@"calendar_changed"];
    
    if (changed || reload)
    {    
        NSLog(@"Reload calendar data...");
        
        if (!firstLoad)
            [[DropBoxHelper Get:self] Push];
        
        [listViewData removeAllObjects];
        
        // SQL
        NSString *query = @"SELECT * FROM calendar ORDER BY date";
        
        sqlite3_stmt* compiledQuery = [[MTISqliteManager sharedInstance] compileQueryOrGetFromCache: query];
        
        while (sqlite3_step(compiledQuery) == SQLITE_ROW)
        {
            int deadlineId = sqlite3_column_int(compiledQuery, 0);
            
            NSString *deadlineName = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(compiledQuery, 1)];
            
            NSString *deadlineDate = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(compiledQuery, 2)];
            
            // Code to do something with extracted data here
            DeadlineModel *model = [[DeadlineModel alloc] init];
            model.deadlineId = deadlineId;
            model.name = deadlineName;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZ"];            
            model.date = [dateFormatter dateFromString:deadlineDate];
            
            [listViewData addObject:model];    
            
            [dateFormatter release];
            [model release];        
            [deadlineName release];
            [deadlineDate release];
        }
        sqlite3_finalize(compiledQuery);
        [listView reloadData];
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"calendar_changed"];
    }
    
    firstLoad = false;
}

#pragma mark DB delegate
- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath from:(NSString*)srcPath{
    NSLog(@"file uploaded %@", destPath);
}
- (void)restClient:(DBRestClient*)client uploadProgress:(CGFloat)progress 
           forFile:(NSString*)destPath from:(NSString*)srcPath{
    //NSLog(@"upload %f", progress);
    
}
- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error{
    
    NSLog(@"error %@", [error description]);
}

- (void)restClient:(DBRestClient *)client loadedFile:(NSString *)destPath
{    
    NSLog(@"file loaded %@", destPath);
}

@end
