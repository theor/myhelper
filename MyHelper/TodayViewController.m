//
//  TodayViewController.m
//  MyHelper
//
//  Created by svp on 20/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TodayViewController.h"

#define CURRENT_CALENDAR [NSCalendar currentCalendar]
#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)

@implementation TodayViewController
@synthesize tableView;
@synthesize nextTableViewData;
@synthesize todayTableViewData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [tableView release];
    [nextTableViewData release];
    [todayTableViewData release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    nextTableViewData = [[NSMutableArray alloc] init];
    todayTableViewData = [[NSMutableArray alloc] init];
    
    self.title = @"Today";
    
    // First load
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"today_changed"];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    bool reload = [[MTISqliteManager sharedInstance] reloadIfNeeded];
    bool changed = [[NSUserDefaults standardUserDefaults] boolForKey:@"today_changed"];
    
    if (changed || reload)
    { 
        NSLog(@"Reload today data...");
        
        [todayTableViewData removeAllObjects];
        [nextTableViewData removeAllObjects];
        
        // SQL
        NSString *query = @"SELECT * FROM calendar ORDER BY date";
        
        sqlite3_stmt* compiledQuery = [[MTISqliteManager sharedInstance] compileQueryOrGetFromCache: query];
        
        while (sqlite3_step(compiledQuery) == SQLITE_ROW)
        {
            int deadlineId = sqlite3_column_int(compiledQuery, 0);
            
            NSString *deadlineName = [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(compiledQuery, 1)];
            
            NSString *deadlineDate = [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(compiledQuery, 2)];
            
            // Code to do something with extracted data here
            DeadlineModel *model = [[DeadlineModel alloc] init];
            model.deadlineId = deadlineId;
            model.name = deadlineName;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZ"];            
            model.date = [dateFormatter dateFromString:deadlineDate];
            
            if ([self isDateToday:model.date])
                [todayTableViewData addObject:model];
            else if ([self isNextDate:model.date])
                [nextTableViewData addObject:model];
            
            [dateFormatter release];
            [model release];        
            [deadlineName release];
            [deadlineDate release];
        }
        sqlite3_finalize(compiledQuery);
        
        [tableView reloadData];
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"today_changed"];
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0 
            ? [todayTableViewData count] 
            : [nextTableViewData count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return section == 0 ? @"Today" : @"Next";
}

/*- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (section == 0)
    {
        if ([todayTableViewData count] == 0)
            return @"No entries for today";
    }
    else
    {
        if ([nextTableViewData count] == 0)
            return @"No entries for the next days";
    }
    return @"";
}*/

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// create the parent view that will hold header Label
	UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 44.0)];
	
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.opaque = NO;
	headerLabel.textColor = [UIColor whiteColor];
	headerLabel.highlightedTextColor = [UIColor yellowColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:20];
	headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 44.0);
    
	// If you want to align the header text as centered
	// headerLabel.frame = CGRectMake(150.0, 0.0, 300.0, 44.0);
    
	headerLabel.text = section == 0 ? @"Today" : @"Next"; // i.e. array element
    
	[customView addSubview:headerLabel];
    
	return customView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	// create the parent view that will hold header Label
	UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 44.0)];
	
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.opaque = NO;
	headerLabel.textColor = [UIColor whiteColor];
	headerLabel.highlightedTextColor = [UIColor yellowColor];
	headerLabel.font = [UIFont systemFontOfSize:18];
	headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 44.0);
    
	// If you want to align the header text as centered
	// headerLabel.frame = CGRectMake(150.0, 0.0, 300.0, 44.0);
    
    if (section == 0)
        if ([todayTableViewData count] == 0)
            headerLabel.text = @"No entries for today";
        else
            headerLabel.text = @"";
        else
            if ([nextTableViewData count] == 0)
                headerLabel.text = @"No entries for the next days";
            else
                headerLabel.text = @"";
    
	[customView addSubview:headerLabel];
    
	return customView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 44.0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 44.0;
}

- (UITableViewCell *)tableView:(UITableView *)argTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    CalendarTableViewCell *cell = (CalendarTableViewCell*)[argTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSArray *components = [[NSBundle mainBundle] loadNibNamed:@"CalendarTableViewCell" owner:nil options:nil];
    if (cell == nil) {
        for (id viewcell in components) {
            if ([viewcell isKindOfClass:[CalendarTableViewCell class]])
            {
                cell = viewcell;
                break;
            }
        }
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    DeadlineModel* model = indexPath.section == 0 
                ? [todayTableViewData objectAtIndex:indexPath.row]
                : [nextTableViewData objectAtIndex:indexPath.row];
    cell.labelOne.text = model.name;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM. yyyy 'at' HH:mm"];
    cell.labelTwo.text = [formatter stringFromDate:model.date];
    
    return cell;
}

- (BOOL) isDateToday: (NSDate *) aDate
{
    NSDateComponents *components1 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:[NSDate date]];
    NSDateComponents *components2 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:aDate];
    return (([components1 year] == [components2 year]) &&
            ([components1 month] == [components2 month]) &&
            ([components1 day] == [components2 day]));
}

- (BOOL) isNextDate: (NSDate *) aDate
{
    NSDateComponents *components1 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:aDate];
    NSDateComponents *components2 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:[NSDate date]];
    return (([components1 year] >= [components2 year]) &&
            ([components1 month] >= [components2 month]) &&
            ([components1 day] >= [components2 day]));
}

@end
