//
//  CalendarAddViewController.h
//  MyHelper
//
//  Created by svp on 20/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarListViewController.h"


@interface CalendarAddViewController : UIViewController {
    
    UIDatePicker *datePicker;
    UITextField *name;
    CalendarListViewController* parent;
}
- (IBAction)saveButtonclick:(id)sender;
- (IBAction)cancelButtonclick:(id)sender;
- (void)setParent:(CalendarListViewController*)newParent;
@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, retain) IBOutlet UITextField *name;

@end
