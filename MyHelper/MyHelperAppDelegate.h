//
//  MyHelperAppDelegate.h
//  MyHelper
//
//  Created by e-artsup on 14/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MTIFileManager.h"
#import "DropboxSDK.h"


@class MyHelperViewController;

@interface MyHelperAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet UITabBarController *viewController;

@end
