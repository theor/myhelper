//
//  DropboxViewController.m
//  MyHelper
//
//  Created by svp on 30/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DropboxViewController.h"

@interface DropboxViewController () <DBLoginControllerDelegate, DBRestClientDelegate,
UITableViewDelegate, UITableViewDataSource>
- (void)LoadInfos;
@end

@implementation DropboxViewController
@synthesize tbl;
@synthesize linkSwitch;
@synthesize colorNames; 

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [colorNames release];
    [linkSwitch release];
    [tbl release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[DBSession sharedSession]isLinked]){
        [self LoadInfos];
    }
    linkSwitch.on = [[DBSession sharedSession]isLinked];
    
    self.colorNames = [[NSMutableDictionary alloc] init];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setLinkSwitch:nil];
    [self setColorNames:nil];
    [self setTbl:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (IBAction)switchChanged:(id)sender {
    DropBoxHelper *dbh = [DropBoxHelper Get:self];
    if (![[DBSession sharedSession] isLinked])
    {
        NSLog(@"Logging");
        [dbh Link:self];
    }
    else
    {
        NSLog(@"Unliking");
        [dbh Unlink];        
    }
}

- (void)loginControllerDidLogin:(DBLoginController*)controller{
    NSLog(@"Logged");
    [self LoadInfos];
}

- (void)loginControllerDidCancel:(DBLoginController*)controller{
    [colorNames removeAllObjects];
    [tbl reloadData];
}

- (void) LoadInfos{
    [[DropBoxHelper Get:self] LoadAccountInfo];
}


- (void)restClient:(DBRestClient*)client loadedAccountInfo:(DBAccountInfo*)info{
    [colorNames removeAllObjects];
    [colorNames setObject:info.email forKey:@"Email"];
    [colorNames setObject:info.displayName forKey:@"Display Name"];
    
    NSString *quota = [[NSString alloc] initWithFormat:@"%llu/%llu", info.quota.totalConsumedBytes/1048576, info.quota.totalBytes/1048576];
    [colorNames setObject:quota forKey:@"Space Used"];
    
    [tbl reloadData];
    
    [[DropBoxHelper Get:self] SqliteMetadata];
}

#pragma mark tableviewdeleagte

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    return [self.colorNames count];
}

- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    CalendarTableViewCell *cell = (CalendarTableViewCell*)[table dequeueReusableCellWithIdentifier:CellIdentifier];
    NSArray *components = [[NSBundle mainBundle] loadNibNamed:@"CalendarTableViewCell" owner:nil options:nil];
    if (cell == nil) {
        for (id viewcell in components) {
            if ([viewcell isKindOfClass:[CalendarTableViewCell class]])
            {
                cell = viewcell;
                break;
            }
        }
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];    
    // Configure the cell.
    NSString *key = [[self.colorNames allKeys] objectAtIndex:[indexPath row]];
    NSString *value = [self.colorNames objectForKey:key];
    cell.labelOne.text = key;
    cell.labelTwo.text = value;
    //NSLog(@"%@ : %@", key, value);//, cell.keyLabel.text, cell.valueLabel.text);
    
    return cell;
}

- (IBAction)pushClick:(id)sender {
    [[DropBoxHelper Get:self] Push];
    [self LoadInfos];
}

- (IBAction)pullClick:(id)sender {
    [[DropBoxHelper Get:self] Pull];
    [self LoadInfos];
    
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"sql_changed"];
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"notes_changed"];
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"calendar_changed"];
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"today_changed"];
}

- (void)restClient:(DBRestClient*)client loadedMetadata:(DBMetadata*)metadata{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM. yyyy 'at' HH:mm"];
    [colorNames setObject:[formatter stringFromDate:metadata.lastModifiedDate] forKey:@"Last push:"];
    [tbl reloadData];
}

- (void)restClient:(DBRestClient*)client loadMetadataFailedWithError:(NSError*)error{
    NSLog(@"%@", error);
}

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath from:(NSString*)srcPath{
    NSLog(@"file uploaded %@", destPath);
}
- (void)restClient:(DBRestClient*)client uploadProgress:(CGFloat)progress 
           forFile:(NSString*)destPath from:(NSString*)srcPath{
    //NSLog(@"upload %f", progress);
    
}
- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error{
    
    NSLog(@"error %@", [error description]);
}

- (void)restClient:(DBRestClient *)client loadedFile:(NSString *)destPath
{    
    NSLog(@"file loaded %@", destPath);
}

@end
