//
//  NotesListViewController.m
//  MyHelper
//
//  Created by svp on 20/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NotesListViewController.h"
#import "NotesAddViewController.h"
#import "NotesDetailsViewController.h"

@interface NotesListViewController () <DBLoginControllerDelegate, DBRestClientDelegate>
@end

@implementation NotesListViewController

@synthesize data;
@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [data dealloc];
    [tableView dealloc];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    firstLoad = true;
    
    self.title = @"Notes";
    self.data = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *sendButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Add", @"")                                                         style:UIBarButtonItemStyleBordered  target:self action:@selector(addButtonclick:)] autorelease];
    
    self.navigationItem.rightBarButtonItem = sendButton;
    
    // First load
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"notes_changed"];
}

- (IBAction)addButtonclick:(id)sender {
    NotesAddViewController* notes = [[NotesAddViewController alloc] init];
    [notes setParent:self];
    [[self navigationController] pushViewController:notes animated:YES];
    [notes clearInputs];
    
    [notes release];
}

- (void)addModel:(NoteModel*)model
{
    [data addObject:model];
    [tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return 100;
    return [data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView2 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *MyIdentifier = @"Cell";    
    
    CalendarTableViewCell *cell = (CalendarTableViewCell*)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    NSArray *components = [[NSBundle mainBundle] loadNibNamed:@"CalendarTableViewCell" owner:nil options:nil];
    if (cell == nil) {
        for (id viewcell in components) {
            if ([viewcell isKindOfClass:[CalendarTableViewCell class]])
            {
                cell = viewcell;
                break;
            }
        }
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    /*
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil) {        
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier] autorelease];        
    }*/
   
    NoteModel *m = [data objectAtIndex:indexPath.row];
    cell.labelOne.text = m.title;
    cell.labelTwo.text = @">";
    /*cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor blackColor];
    cell.textLabel.textColor = [UIColor whiteColor];*/
    
    return cell;
}

- (void)tableView:(UITableView *)tableView2 didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NoteModel *m = [data objectAtIndex:indexPath.row];
    
    NotesDetailsViewController *details = [[NotesDetailsViewController alloc] init];
    details.model = m;
    [[self navigationController] pushViewController:details animated:YES];
    [details release];
}

- (void)tableView:(UITableView *)tableView2 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NoteModel *model = [data objectAtIndex:indexPath.row];
        NSString *query = @"DELETE FROM notes where id = ? ";
        
        sqlite3_stmt* compiledQuery = [[MTISqliteManager sharedInstance] compileQuery:query];
        sqlite3_bind_int(compiledQuery, 1, model.noteId);
        sqlite3_step(compiledQuery);
        sqlite3_finalize(compiledQuery);
        
        [data removeObject:model];
        [tableView reloadData];
        
        [[DropBoxHelper Get:self] Push];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    bool reload = [[MTISqliteManager sharedInstance] reloadIfNeeded];
    
    bool changed = [[NSUserDefaults standardUserDefaults] boolForKey:@"notes_changed"];
    
    if (changed || reload)
    {    
        NSLog(@"Reload notes data...");
        
        if (!firstLoad)
            [[DropBoxHelper Get:self] Push];
        
        [data removeAllObjects];
        
        // SQL
        NSString *query = @"SELECT id, title, description FROM notes";
        
        sqlite3_stmt* compiledQuery = [[MTISqliteManager sharedInstance] compileQueryOrGetFromCache: query];
        
        while (sqlite3_step(compiledQuery) == SQLITE_ROW)
        {
            int noteId = sqlite3_column_int(compiledQuery, 0);
            
            NSString *noteTitle = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(compiledQuery, 1)];
            
            NSString *noteDescription = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(compiledQuery, 2)];
            
            // Code to do something with extracted data here
            NoteModel *model = [[NoteModel alloc] init];
            model.noteId = noteId;
            model.title = noteTitle;
            model.description = noteDescription;
            
            [data addObject:model];        
            
            [model release];        
            [noteTitle release];
            [noteDescription release];
        }
        sqlite3_finalize(compiledQuery);
        
        [tableView reloadData];
        
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"notes_changed"];
    }
    
    firstLoad = false;
}

- (void)viewDidUnload
{
    self.data = nil;
    self.tableView = nil;
    
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark DB delegate
- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath from:(NSString*)srcPath{
    NSLog(@"file uploaded %@", destPath);
}
- (void)restClient:(DBRestClient*)client uploadProgress:(CGFloat)progress 
           forFile:(NSString*)destPath from:(NSString*)srcPath{
    //NSLog(@"upload %f", progress);
    
}
- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error{
    
    NSLog(@"error %@", [error description]);
}

- (void)restClient:(DBRestClient *)client loadedFile:(NSString *)destPath
{    
    NSLog(@"file loaded %@", destPath);
}

@end
