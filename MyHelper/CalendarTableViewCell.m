//
//  CalendarTableViewCell.m
//  MyHelper
//
//  Created by svp on 04/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CalendarTableViewCell.h"


@implementation CalendarTableViewCell
@synthesize labelTwo;
@synthesize labelOne;

- (void)dealloc {
    [labelTwo release];
    [labelOne release];
    [super dealloc];
}
@end
