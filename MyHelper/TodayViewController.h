//
//  TodayViewController.h
//  MyHelper
//
//  Created by svp on 20/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarTableViewCell.h"
#import "DeadlineModel.h"
#import "MTISqliteManager.h"

@interface TodayViewController : UIViewController {
    UITableView *tableView;
    NSMutableArray *nextTableViewData;
    NSMutableArray *todayTableViewData;
}
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutletCollection(UITableView) NSMutableArray *nextTableViewData;
@property (nonatomic, retain) IBOutletCollection(UITableView) NSMutableArray *todayTableViewData;

- (BOOL) isDateToday: (NSDate *) aDate;
- (BOOL) isNextDate: (NSDate *) aDate;

@end
