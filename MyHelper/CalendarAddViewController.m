//
//  CalendarAddViewController.m
//  MyHelper
//
//  Created by svp on 20/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CalendarAddViewController.h"

@interface CalendarAddViewController () <UITextFieldDelegate>
@end

@implementation CalendarAddViewController
@synthesize datePicker;
@synthesize name;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [datePicker release];
    [name release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Add an event";
    
    UIBarButtonItem *saveButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", @"") style:UIBarButtonItemStyleBordered  target:self action:@selector(saveButtonclick:)] autorelease];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    UIBarButtonItem *cancelButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", @"") style:UIBarButtonItemStyleBordered  target:self action:@selector(cancelButtonclick:)] autorelease];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    datePicker.date = [NSDate date];
}

- (void)viewDidUnload
{
    [self setDatePicker:nil];
    [self setName:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)saveButtonclick:(id)sender {
    
    [parent addEvent:[[DeadlineModel alloc] initWithData:0 name:[name text] date:[datePicker date]]];
    
    [[self navigationController] popToRootViewControllerAnimated:YES];
}


- (void)setParent:(CalendarListViewController*)newParent
{
    parent = newParent;
}

- (IBAction)cancelButtonclick:(id)sender {
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
