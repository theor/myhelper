//
//  MTIUtils.m
//  TP1
//
//  Created by Plopix on 11/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MTIUtils.h"


@implementation MTIUtils

+ (NSString*) md5:(NSString *)str {
	const char *cStr = [str UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5( cStr, strlen(cStr), result );
	return [NSString stringWithFormat:
			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]
			];
} 
+ (NSString *) md5Minuscule:(NSString*) string {
	NSString *md5 = [MTIUtils md5:string];
	md5 = [md5 stringByReplacingOccurrencesOfString:@"A" withString:@"a"];
	md5 = [md5 stringByReplacingOccurrencesOfString:@"B" withString:@"b"];
	md5 = [md5 stringByReplacingOccurrencesOfString:@"C" withString:@"c"];
	md5 = [md5 stringByReplacingOccurrencesOfString:@"D" withString:@"d"];
	md5 = [md5 stringByReplacingOccurrencesOfString:@"E" withString:@"e"];
	md5 = [md5 stringByReplacingOccurrencesOfString:@"F" withString:@"f"];
	return md5;
}


@end
