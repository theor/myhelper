//
//  NotesAddViewController.h
//  MyHelper
//
//  Created by svp on 20/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MTISqliteManager.h"
#import "NotesListViewController.h"
#import "NoteModel.h"

@interface NotesAddViewController : UIViewController {
    NotesListViewController *parent;
}

@property (nonatomic, retain) NoteModel *model;
@property (nonatomic, retain) IBOutlet UITextField* titleField;
@property (nonatomic, retain) IBOutlet UITextView* textView;

- (IBAction)saveButtonclick:(id)sender;
- (void)setParent:(NotesListViewController*)parent;
- (void)clearInputs;

// TODO: Remplacer parent avec une date de modif dans NSUserDefaults + 
// reloadSQL + reloadData dans viewWillAppear

@end
