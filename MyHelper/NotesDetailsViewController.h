//
//  NotesDetailsViewController.h
//  MyHelper
//
//  Created by svp on 20/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NoteModel.h"

#import "NotesListViewController.h"
#import "NotesAddViewController.h"

@interface NotesDetailsViewController : UIViewController {
}

@property (nonatomic, retain) NoteModel *model;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UITextView *textView;

- (IBAction)editButtonclick:(id)sender;

@end
