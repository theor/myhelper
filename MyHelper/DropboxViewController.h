//
//  DropboxViewController.h
//  MyHelper
//
//  Created by svp on 30/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropBoxHelper.h"
#import "CalendarTableViewCell.h"



@interface DropboxViewController : UIViewController {
    
    UISwitch *linkSwitch;
    NSMutableDictionary *colorNames;
    UITableView *tbl;
}
@property (nonatomic, retain) IBOutlet UISwitch *linkSwitch;
@property (nonatomic, retain) NSMutableDictionary *colorNames;
- (IBAction)switchChanged:(id)sender;
@property (nonatomic, retain) IBOutlet UITableView *tbl;
- (IBAction)pushClick:(id)sender;
- (IBAction)pullClick:(id)sender;

@end
