//
//  MTIFileManager.h
//  TP1
//
//  Created by Plopix on 11/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum ApplicationPath {
	APP_DOCUMENTS,	
	APP_TEMP
} ApplicationPath;



@interface MTIFileManager : NSObject {
    
}


+ (NSString *)getPathIn: (ApplicationPath) path;
+ (NSString *) fullPath: (NSString *) fileName In: (ApplicationPath) path;
+ (BOOL)exists: (NSString *) fileName In: (ApplicationPath) path;	
+ (void) remove: (NSString *) itemName In: (ApplicationPath) path;
+ (void) rename: (NSString *) sourceName to: (NSString *) destName In: (ApplicationPath) path;



+ (void) cacheDictionnary: (NSString *) dictName withDict:(NSMutableDictionary*) dict In: (ApplicationPath) path;	
+ (NSMutableDictionary *) getCachedDictionnary:(NSString *) dictName In: (ApplicationPath) path;	
+ (void) cacheImage: (NSString *) imageName withData:(NSMutableData*) data In: (ApplicationPath) path;	
+ (UIImage *) getCachedImage: (NSString *) imageName In: (ApplicationPath) path;


@end
