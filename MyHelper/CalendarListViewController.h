//
//  CalendarListViewController.h
//  MyHelper
//
//  Created by svp on 21/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropBoxHelper.h"
#import "DeadlineModel.h"
#import "CalendarTableViewCell.h"
#import "MTISqliteManager.h"


@interface CalendarListViewController : UIViewController {
    
    UITableView *listView;
    bool firstLoad;
    //NSMutableArray *listViewData;
}
@property (nonatomic, retain) IBOutlet UITableView *listView;
@property (nonatomic, retain) NSMutableArray *listViewData;

- (IBAction)addButtonclick:(id)sender;
- (IBAction)addEvent:(DeadlineModel*)deadline;

@end
