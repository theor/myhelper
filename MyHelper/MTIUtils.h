//
//  MTIUtils.h
//  TP1
//
//  Created by Plopix on 11/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface MTIUtils : NSObject {
 
}

+ (NSString *) md5:(NSString*) string;
+ (NSString *) md5Minuscule:(NSString*) string;

@end
