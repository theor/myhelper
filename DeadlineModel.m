//
//  DeadlineModel.m
//  MyHelper
//
//  Created by svp on 22/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DeadlineModel.h"

@implementation DeadlineModel

@synthesize deadlineId;
@synthesize name;
@synthesize date;

- (id)initWithData:(int)newId name:(NSString*)newName date:(NSDate*)newDate
{
    DeadlineModel *model = [[DeadlineModel alloc] init];
    model.deadlineId = newId;
    model.name = newName;
    model.date = newDate;
    return model;
}

@end
